const mongoose = require('mongoose');
require('dotenv').config()

const app = require('./app');

const DB = process.env.DATABASE.replace(
    '<password>',
    process.env.DATABASE_PASSWORD
  ).replace('<db_user>' , process.env.DATABASE_USER).replace('<db_name>',process.env.DATABASE_NAME);
  

  mongoose
  .connect(DB, {
    useNewUrlParser: true, 
    useUnifiedTopology: true 
  })
  .then(() => console.log('DB connection successful!'));


  const port = process.env.APP_PORT || 3000;
  const server = app.listen(port, () => {
  console.log(`App running on port ${port}...`);
});