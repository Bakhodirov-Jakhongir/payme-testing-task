const mongoose = require('mongoose');

const employeeSchema = new mongoose.Schema({
    fio : {
        type:String,
        required:[true , 'Please fill in your fio']
    },
    lavozimi: {
        type:String,
        required:[true , 'Please provide your position']
    },
    otdel:{
        type:String,
        required:[true , 'Please provide your department']
    },
    addresses: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Address'
     }]
})

const Employee = mongoose.model('Employee' , employeeSchema);

module.exports = Employee