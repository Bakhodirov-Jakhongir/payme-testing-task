const mongoose = require('mongoose');


const addressSchema = new mongoose.Schema({
    address : {
        type:String,
        required:[true , 'Please fill in your address']
    },
    employee:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Employee',
        required:true
    }
})

const Address = mongoose.model('Address' , addressSchema);

module.exports = Address