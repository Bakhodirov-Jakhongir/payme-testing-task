const catchAsync = require('./../utils/catchAsync');
const Employee = require('./../models/employeeModel');

exports.getAllEmployees = catchAsync(async (req, res, next) => {
  const employees = await Employee.find();

  // SEND RESPONSE
  res.status(200).json({
    status: 'success',
    results: employees.length,
    data: {
        employees
    }
  });
});


exports.getEmployee = (req, res) => {
    const id = req.params.id;

    Employee.findById(id)
      .then(data => {
        if (!data)
          res.status(404).send({ message: "Not found Employee with id " + id });
        else res.send(data);
      })
      .catch(err => {
        res
          .status(500)
          .send({ message: "Error retrieving Employee with id=" + id });
      });
};

exports.createEmployee = catchAsync(async(req, res , next) => {
  const newEmployee = await Employee.create(req.body);
  res.status(201).json({
    status: 'success',
    data:{
      employee:newEmployee
    }
  });
});

exports.updateEmployee = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
          message: "Data to update can not be empty!"
        });
      }
    
      const id = req.params.id;
    
      Employee.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
          if (!data) {
            res.status(404).send({
              message: `Cannot update Employee with id=${id}. Maybe Employee was not found!`
            });
          } else res.send({ message: "Employee was updated successfully." });
        })
        .catch(err => {
          res.status(500).send({
            message: "Error updating Employee with id=" + id
          });
        });
};

exports.deleteEmployee = (req, res) => {
    const id = req.params.id;

    Employee.findByIdAndRemove(id)
      .then(data => {
        if (!data) {
          res.status(404).send({
            message: `Cannot delete Employee with id=${id}. Maybe Employee was not found!`
          });
        } else {
          res.send({
            message: "Employee was deleted successfully!"
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Employee with id=" + id
        });
      });
};

